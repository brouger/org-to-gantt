# org-to-gantt

## Presentation

Org-to-Gantt is a Python3 script that uses [python-gantt](https://pypi.org/project/python-gantt/)
(https://pypi.org/project/python-gantt/) to create Gantt charts from
org files.

It is was designed to fit my specific needs in terms of Gantt charts
creation, and might need adaptation to fit yours.

## Creation of the diagram

It is easy to run, once you installed the python-gantt dependency with
pip.

1. Change the name of your org file in the `orgToGantt.py` file
2. Run `orgToGantt.py` using `python3 orgToGantt.py`
3. Run the produced Python script that contains the instruction for
   the Gantt diagram creation using `python3 myGantt.py`

## Use

Tasks groups names are preceded by `*` as org uses for headlines.

Tasks names are preceded by `* TODO`

You can then specify properties for your tasks groups and tasks.

### Output
`python-gantt` output gantt charts in svg format.

I personnally chose to output the following graphs :
- chart of all tasks
- chart of all tasks with a mountly scale
- chart of all tasks with a weekly scale
- chart of all resources

### Task groups properties

There is only one property so far for the task groups. I usually write it between `:PROPERTIES:` and `:END:` as I use org's keybind to add those properties.

#### `:ORDERED:`
This keyword is not mandatory. It allows to tell `python-gantt` that this task group is inside an other task group.

It is followed by a numeric argument that means of how many task groups you have to go up to be included in the correct task group.

This system is not as dynamic as org is, but I hadn't timeto figure a better way to do it so far.

For example :
``` org
* Big task group
** Sub-task group 1
   :PROPERTIES:
   :ORDERED:   1
   :END:
** Sub-task group 2
   :PROPERTIES:
   :ORDERED:   2
   :END:
*** Sub-sub task group
   :PROPERTIES:
   :ORDERED:   1
   :END:
```

will include both Sub-task group 1 and 2 in Big task group, and will include Sub-sub task group in Sub-task group 2

My script does not take into account the number of `*` in front of the mane to determine nesting.

### Tasks properties
At least one keyword is required to create a task : either the `:SCHEDULED:` or the `:DEADLINE:` keyword.

Default length of task is 1 day. Please note that in `python-gantt`, you can not specify task length under 1 day.

You can use `[#A]`, `[#B]` and `[#C]` to make the task appear in red, orange and yellow respectively.

#### `:SCHEDULED:`

This keyword is followed by a date included by org mode as `<YYYY-MM-DD day>`.

It allows to set the first day at which the task can start.

#### `:DEADLINE:`

This keyword is followed by a date included by org mode as `<YYYY-MM-DD day>`.

It allows to set date at which the task must be finished.

Note that `:DEADLINE:` has priority on `:SCHEDULED:` due to `python-gantt`

#### `:Effort:`

This keyword is followed by a time duration. It is composed of a numerical part a a character indicating if the value is in days (d), weeks (w) or mounths (m).

It allows to set the estimated duration of a task.

For example, `:Effort: 7d` sets the duration of the task to seven days.

#### `:PROGRESS:`

This keyword is followed by a numerical value between 0 and 100.

It allows to report the progress on the task.

For example, `:PROGRESS: 25` means that 25% of the task has been done.

#### `:ORDERED:`

This keyword is followed by `t` when present. It allows to specify that this task depends on the previous one

#### `:RESOURCES:`

This keyword is followed by string value(s) to specify who has to do the task, separated by `, `

The resource list is incremented while parsing the org file.

Fro example, `:RESOURCES: MAN1, WOMAN2` will assign the task to Man1 and Woman2
