#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime
import gantt


MAN1 = gantt.Resource('MAN1')
WOMAN2 = gantt.Resource('WOMAN2')
WOMAN1 = gantt.Resource('WOMAN1')


p = gantt.Project(name= ' ')
p3 = gantt.Project(name = "Project 1", color='#c9a396')
p.add_task(p3)
p2 = gantt.Project(name = "Project 2", color='#994e53')
p.add_task(p2)
p1 = gantt.Project(name = "Project 3", color='#950feb')
p2.add_task(p1)
t5 = gantt.Task(name = "Task 1", start = datetime.date(2020, 3, 23), duration = 7, resources=[MAN1])
p3.add_task(t5)
t4 = gantt.Task(name = "Task 2", stop = datetime.date(2020, 3, 26), duration = 15, depends_of = t5, color = "#66FF99", resources=[MAN1, WOMAN1], percent_done=75)
p3.add_task(t4)
t3 = gantt.Task(name = "Tache 3", start = datetime.date(2020, 4, 17), duration = 2, resources=[MAN1, WOMAN2])
p2.add_task(t3)
t2 = gantt.Task(name = "Tache 4", start = datetime.date(2020, 4, 22), duration = 2, depends_of = t3, color = "#FFCC00")
p1.add_task(t2)
t1 = gantt.Task(name = "Tache 5", start = datetime.date(2020, 5, 14), duration = 3, color = "#CC0000", percent_done=50)
p1.add_task(t1)
p.make_svg_for_tasks(filename='myGantt.svg', today=datetime.date.today(), start = p.start_date() - datetime.timedelta(days=7), end = p.end_date() + datetime.timedelta(days=2))
p.make_svg_for_tasks(filename='myGanttMonth.svg', today=datetime.date.today(), start = p.start_date() - datetime.timedelta(days=7), end = p.end_date() + datetime.timedelta(days=2), scale=gantt.DRAW_WITH_MONTHLY_SCALE)
p.make_svg_for_tasks(filename='myGanttWeek.svg', today=datetime.date.today(), start = p.start_date() - datetime.timedelta(days=7), end = p.end_date() + datetime.timedelta(days=2), scale=gantt.DRAW_WITH_WEEKLY_SCALE)
p.make_svg_for_resources(filename='myGanttResources.svg', today=datetime.date.today(), start = p.start_date() - datetime.timedelta(days=7), end = p.end_date() + datetime.timedelta(days=2), resources=[MAN1, WOMAN2, WOMAN1])