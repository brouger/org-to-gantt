import hashlib
from pathlib import Path

FILE_PATH = "orgExample.org"

CONTENTS = Path(FILE_PATH).read_text()

CONTENTSsplitted = CONTENTS.split("*")

CONTENTScleaned = [i for i in CONTENTSsplitted if len(i) > 0]

#COLORS = ['#ffcc00', '#852121', '#50ffd8', '#ff80a2', '#ff0081', '#7c00e7', '#f4b3a5', '#ffe4e1', '#ff7777', '#f2e571', '#3e4e55', '#b967ff', '#05ffa1', '#01cdfe', '#ff71ce', '#f29e55', '#a8ca2e', '#70258c', '#3399ff', '#8a2be2', '#3399ff', '#00ff7f', '#ccff00', '#00ced1', '#daa520', '#008000', '#800080', '#003366', '#ffa500', '#ffd700', '#5ac18e', '#f7347a', '#065535', '#ff0000', '#bada55', '#ffddff', '#f28db3', '#69edde', '#54976b', '#784a62', '#ed5168', '#5d3455']

#COLORS = ['#000000', '#0C090A', '#2C3539', '#2B1B17', '#34282C', '#25383C', '#3B3131', '#413839', '#3D3C3A', '#463E3F', '#4C4646', '#504A4B', '#565051', '#5C5858', '#625D5D', '#666362', '#6D6968', '#726E6D', '#736F6E', '#837E7C', '#848482', '#B6B6B4', '#D1D0CE', '#E5E4E2', '#BCC6CC', '#98AFC7', '#6D7B8D', '#657383', '#616D7E', '#646D7E', '#566D7E', '#737CA1', '#4863A0', '#2B547E', '#2B3856', '#151B54', '#000080', '#342D7E', '#15317E', '#151B8D', '#0000A0', '#0020C2', '#0041C2', '#2554C7', '#1569C7', '#2B60DE', '#1F45FC', '#6960EC', '#736AFF', '#357EC7', '#368BC1', '#488AC7', '#3090C7', '#659EC7', '#87AFC7', '#95B9C7', '#728FCE', '#2B65EC', '#306EFF', '#157DEC', '#1589FF', '#6495ED', '#6698FF', '#38ACEC', '#56A5EC', '#5CB3FF', '#3BB9FF', '#79BAEC', '#82CAFA', '#82CAFF', '#A0CFEC', '#B7CEEC', '#B4CFEC', '#C2DFFF', '#C6DEFF', '#AFDCEC', '#ADDFFF', '#BDEDFF', '#CFECEC', '#E0FFFF', '#EBF4FA', '#F0F8FF', '#F0FFFF', '#CCFFFF', '#93FFE8', '#9AFEFF', '#7FFFD4', '#00FFFF', '#7DFDFE', '#57FEFF', '#8EEBEC', '#50EBEC', '#4EE2EC', '#81D8D0', '#92C7C7', '#77BFC7', '#78C7C7', '#48CCCD', '#43C6DB', '#46C7C7', '#7BCCB5', '#43BFC7', '#3EA99F', '#3B9C9C', '#438D80', '#348781', '#307D7E', '#5E7D7E', '#4C787E', '#008080', '#4E8975', '#78866B', '#848b79', '#617C58', '#728C00', '#667C26', '#254117', '#306754', '#347235', '#437C17', '#387C44', '#347C2C', '#347C17', '#348017', '#4E9258', '#6AA121', '#4AA02C', '#41A317', '#3EA055', '#6CBB3C', '#6CC417', '#4CC417', '#52D017', '#4CC552', '#54C571', '#99C68E', '#89C35C', '#85BB65', '#8BB381', '#9CB071', '#B2C248', '#9DC209', '#A1C935', '#7FE817', '#59E817', '#57E964', '#64E986', '#5EFB6E', '#00FF00', '#5FFB17', '#87F717', '#8AFB17', '#6AFB92', '#98FF98', '#B5EAAA', '#C3FDB8', '#CCFB5D', '#B1FB17', '#BCE954', '#EDDA74', '#EDE275', '#FFE87C', '#FFFF00', '#FFF380', '#FFFFC2', '#FFFFCC', '#FFF8C6', '#FFF8DC', '#F5F5DC', '#FBF6D9', '#FAEBD7', '#F7E7CE', '#FFEBCD', '#F3E5AB', '#ECE5B6', '#FFE5B4', '#FFDB58', '#FFD801', '#FDD017', '#EAC117', '#F2BB66', '#FBB917', '#FBB117', '#FFA62F', '#E9AB17', '#E2A76F', '#DEB887', '#FFCBA4', '#C9BE62', '#E8A317', '#EE9A4D', '#C8B560', '#D4A017', '#C2B280', '#C7A317', '#C68E17', '#B5A642', '#ADA96E', '#C19A6B', '#CD7F32', '#C88141', '#C58917', '#AF9B60', '#AF7817', '#B87333', '#966F33', '#806517', '#827839', '#827B60', '#786D5F', '#493D26', '#483C32', '#6F4E37', '#835C3B', '#7F5217', '#7F462C', '#C47451', '#C36241', '#C35817', '#C85A17', '#CC6600', '#E56717', '#E66C2C', '#F87217', '#F87431', '#E67451', '#FF8040', '#F88017', '#FF7F50', '#F88158', '#F9966B', '#E78A61', '#E18B6B', '#E77471', '#F75D59', '#E55451', '#E55B3C', '#FF0000', '#FF2400', '#F62217', '#F70D1A', '#F62817', '#E42217', '#E41B17', '#DC381F', '#C34A2C', '#C24641', '#C04000', '#C11B17', '#9F000F', '#990012', '#8C001A', '#954535', '#7E3517', '#8A4117', '#7E3817', '#800517', '#810541', '#7D0541', '#7E354D', '#7D0552', '#7F4E52', '#7F5A58', '#7F525D', '#B38481', '#C5908E', '#C48189', '#C48793', '#E8ADAA', '#ECC5C0', '#EDC9AF', '#FDD7E4', '#FCDFFF', '#FFDFDD', '#FBBBB9', '#FAAFBE', '#FAAFBA', '#F9A7B0', '#E7A1B0', '#E799A3', '#E38AAE', '#F778A1', '#E56E94', '#F660AB', '#FC6C85', '#F6358A', '#F52887', '#E45E9D', '#E4287C', '#F535AA', '#FF00FF', '#E3319D', '#F433FF', '#D16587', '#C25A7C', '#CA226B', '#C12869', '#C12267', '#C25283', '#C12283', '#B93B8F', '#7E587E', '#571B7E', '#583759', '#4B0082', '#461B7E', '#4E387E', '#614051', '#5E5A80', '#6A287E', '#7D1B7E', '#A74AC7', '#B048B5', '#6C2DC7', '#842DCE', '#8D38C9', '#7A5DC7', '#7F38EC', '#8E35EF', '#893BFF', '#8467D7', '#A23BEC', '#B041FF', '#C45AEC', '#9172EC', '#9E7BFF', '#D462FF', '#E238EC', '#C38EC7', '#C8A2C8', '#E6A9EC', '#E0B0FF', '#C6AEC7', '#F9B7FF', '#D2B9D3', '#E9CFEC', '#EBDDE2', '#E3E4FA', '#FDEEF4', '#FFF5EE', '#FEFCFF', '#FFFFFF']

COLORS = ['#ff0000', '#ff4000', '#ff8000', '#ffbf00', '#ffff00', '#bfff00', '#80ff00', '#40ff00', '#00ff00', '#00ff40', '#00ff80', '#00ffbf', '#00ffff', '#00bfff', '#0080ff', '#0040ff', '#0000ff', '#4000ff', '#8000ff', '#bf00ff', '#ff00ff', '#ff00bf', '#ff0080', '#ff0040', '#ff0000']

# print(CONTENTS.split("*"))

RESSOURCES = []

FILETOPRINT = []

PROJ_COUNTER = 1
TASK_COUNTER = 1


def pick_color(title):
    a = str(hex(int(hashlib.sha1(title.encode('utf-8')).hexdigest(), 16) * len(title) % (16377215)))
    while len(a) < 8:
        a = a[0:2] + '0' + a[2:]
    a = '#' + a[2:]
    return a


def convert_to_date(string):
    a = string.split(":")
    b = a[len(a)-1].strip().split(" ")[0].split("<")[1].split("-")
    c = ", ".join([str(int(i)) for i in b])
    return c


def simplify_date(string):
    a = string.split("-")
    b = ", ".join([str(int(i)) for i in a])
    return b


def convert_effort(string):
    a = string.split("\n")
    effort = [i.strip() for i in a if "Effort" in i][0].split(":")[-1].strip()
    if "d" in effort:
        duration = effort.split("d")[0]
    elif "w" in effort:
        duration = str(int(effort.split("w")[0]) * 5)
    elif "m" in effort:
        duration = str(int(effort.split("m")[0]) * 20)
    return duration


#def startdate_from_deadline(string):


def add_project(task_string, proj_counter):
    task_splitted = task_string.split("\n")
    project_name = task_splitted[0].strip()
    print(project_name)
    # print(task_string)
    try:
        project_ordered = task_string.split("ORDERED: ")[1].strip().split("\n")[0].strip()
        # print(type(project_ordered))
    except:
        project_ordered = False
    output_file = "p" + str(proj_counter) + " = gantt.Project(name = \"" + project_name + "\", color='" + pick_color(project_name) + "')\n"
    if "ORDERED" in task_string:
        # print(project_ordered)
        output_file += "p" + str(proj_counter + int(project_ordered)) + ".add_task(p" + str(proj_counter) + ")\n"
    else:
        output_file += "p.add_task(p" + str((proj_counter)) + ")\n"
    return output_file


def add_task(task_string, task_counter, proj_counter):
    task_splitted = task_string.split("\n")
    task_splitted = [task.strip() for task in task_splitted]
    task_name = task_splitted[0].split("TODO ")[1]
    if "]" in task_name:
        task_name = task_name.split("]")[1].strip()
    if "DEADLINE" in task_string:
        task_deadline = convert_to_date([task for task in task_splitted if "DEADLINE" in task][0])
    elif "SCHEDULED" in task_string:
        task_scheduled = convert_to_date([task for task in task_splitted if "SCHEDULED" in task][0])
    # print(output_file)
    output_file = "t" + str(task_counter) + " = gantt.Task(name = \"" + task_name + "\""
    if "DEADLINE" in task_string:
        output_file += ", stop = datetime.date(" + task_deadline
    elif "SCHEDULED" in task_string:
        output_file += ", start = datetime.date(" + task_scheduled
    output_file += "), duration = " + convert_effort(task_string)

    if "ORDERED: t" in task_string:
        output_file += ", depends_of = t" + str(task_counter + 1)

    if "[#A]" in task_string:
        output_file += ", color = \"#CC0000\""
    elif "[#B]" in task_string:
        output_file += ", color = \"#FFCC00\""
    elif "[#C]" in task_string:
        output_file += ", color = \"#66FF99\""

    if "RESOURCES" in task_string:
        a = task_string.split("RESOURCES: ")[1].split("\n")[0]
        output_file += ", resources=[" + a + "]"
        global RESSOURCES
        b = a.split(", ")
        for ress in b:
            if ress not in RESSOURCES:
                RESSOURCES.append(ress)

    if "PROGRESS" in task_string:
        a = task_string.split("PROGRESS: ")[1].split("\n")[0]
        output_file += ", percent_done=" + a
    output_file += ")\np" + str(proj_counter) + ".add_task(t" + str(task_counter) + ")\n"
    # print(output_file)
    return output_file


for item in reversed(CONTENTScleaned):
    # print(item)
    if "TODO" in item:
        # print("hi")
        FILETOPRINT.append(add_task(item, TASK_COUNTER, PROJ_COUNTER))
        TASK_COUNTER += 1
    else:
        # print("ho")
        FILETOPRINT.append(add_project(item, PROJ_COUNTER))
        PROJ_COUNTER += 1


# FILETOPRINT.append("#!/usr/bin/env python3\n# -*- coding: utf-8 -*-\n\nimport datetime\nimport gantt\n\n\n")
# print(FILETOPRINT)

HEADER = "#!/usr/bin/env python3\n# -*- coding: utf-8 -*-\n\nimport datetime\nimport gantt\n\n\n"

PROJ_LIST = [p for p in FILETOPRINT if p[0] == "p"]
TASK_LIST = [t for t in FILETOPRINT if t[0] == "t"]

# print(TASK_LIST)

# filetoprint += "\n\n\np = gantt.Project(name= ' ')\n"
# for i in reversed(range(PROJ_COUNTER-1)):
#     # print(FILETOPRINT)
#     PROJ_LIST.insert(0,"p.add_task(p" + str((i+1)) + ")\n")

PROJ_LIST.append("\n\np = gantt.Project(name= ' ')\n")
# print(PROJ_LIST)

# FILETOPRINT += "p.make_svg_for_tasks(filename='test_auto.svg', today=datetime.date(2020, 1, 14))"

F = open("myOrg.py", "w")
F.write(HEADER)
F.close()

F = open("myOrg.py", "a")
for ress in RESSOURCES:
    ligne = ress + " = gantt.Resource('" + ress + "')\n"
    F.write(ligne)
F.close()

F = open("myOrg.py", "a")
for proj in reversed(PROJ_LIST):
    F.write(proj)
F.close()

F = open("myOrg.py", "a")
for task in reversed(TASK_LIST):
    F.write(task)
F.close()

# AUJ = simplify_date(datetime.date.today().isoformat())
# DEB = "datetime.date(" + simplify_date((p.start_date() - datetime.timedelta(days=2)).isoformat()) + ")"
# FIN = "datetime.date(" + simplify_date((p.start_date() + datetime.timedelta(days=2)).isoformat()) + ")"

F = open("myOrg.py", "a")
F.write("p.make_svg_for_tasks(filename='myGantt.svg', today=datetime.date.today(), start = p.start_date() - datetime.timedelta(days=7), end = p.end_date() + datetime.timedelta(days=2))\np.make_svg_for_tasks(filename='myGanttMonth.svg', today=datetime.date.today(), start = p.start_date() - datetime.timedelta(days=7), end = p.end_date() + datetime.timedelta(days=2), scale=gantt.DRAW_WITH_MONTHLY_SCALE)\np.make_svg_for_tasks(filename='myGanttWeek.svg', today=datetime.date.today(), start = p.start_date() - datetime.timedelta(days=7), end = p.end_date() + datetime.timedelta(days=2), scale=gantt.DRAW_WITH_WEEKLY_SCALE)\np.make_svg_for_resources(filename='myGanttResources.svg', today=datetime.date.today(), start = p.start_date() - datetime.timedelta(days=7), end = p.end_date() + datetime.timedelta(days=2), resources=[" + ", ".join(RESSOURCES) + "])")
F.close()
